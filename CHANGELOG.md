## 0.3.2 (November 29th, 2019) ##

* Change va_total_amount expectation from String to Numeric

## 0.3.1 (November 29th, 2019) ##

* Make sure first parameter of from_amount is Numeric when check for 'equal_amount?'

## 0.3.0 (November 29th, 2019) ##

* Add va_currency and expect va_total_amount as String
* Save request params and response body as stringified JSON

## 0.2.6 (October 22nd, 2019) ##

* Fix from_amount parameter as Numeric
* Change some method name to be more meaningful

## 0.2.5 (October 21st, 2019) ##

* Use Money#from_amount to fix invalid amount validation

## 0.2.4 (October 18th, 2019) ##

* Move RequestHistory model and load if not skip_history while install
* Convert va_total_amount to Numeric first if possible

## 0.2.3 (October 17th, 2019) ##

* Fix validation and generator
* Remove unnecessary concern file
* Add 'money' gem and validate amount_value between va_total_amount and total_amount in money
* Add missing translation for :no_match error message

## 0.2.2 (October 16th, 2019) ##

* Add feature to whitelist ip before access
* Add whitelist_ip for Bca adapter
* Add whitelisted_ips config for Bovaga initializer

## 0.2.1 (October 16th, 2019) ##

* Update README.md with 'Getting Started' instruction
* Create controller to override when generate Bovaga models

## 0.2.0 (October 15th, 2019)  ##

* Initial attempt for Bovaga
* Apply bca as bank adapter
