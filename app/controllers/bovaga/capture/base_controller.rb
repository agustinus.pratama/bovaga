# frozen_string_literal: true

module Bovaga
  module Capture
    class BaseController < BovagaController
      def bill
      end

      def payment
      end
    end
  end
end
