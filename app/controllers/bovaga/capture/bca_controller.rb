# frozen_string_literal: true

module Bovaga
  module Capture
    class BcaController < BaseController
      include Bovaga::Controllers::RequestHistoryConcern

      before_action :validate_transaction_date

      def bill
        bill = bill_klass.constantize.new(bill_params)
        response = bill_response_klass.constantize.new(bill)

        response.process do
          after_bill_success(bill)
        end

        create_history(
          bank_code: 'BCA',
          kind_of: :bill,
          request: params,
          request_id: bill.request_id,
          response: response.body,
          messages: bill.all_error_messages)

        render json: response.body
      end

      def payment
        payment = payment_klass.constantize.new(payment_params)
        response = payment_response_klass.constantize.new(payment)

        response.process do
          after_payment_success(payment)
        end

        create_history(
          bank_code: 'BCA',
          kind_of: :payment,
          request: params,
          request_id: payment.request_id,
          response: response.body,
          messages: payment.all_error_messages)

        render json: response.body
      end

      protected

      def bill_klass
        'Bovaga::Bca::Bill'
      end

      def after_bill_success(bill)
      end

      def payment_klass
        'Bovaga::Bca::Payment'
      end

      def after_payment_success(payment)
      end

      private

      def bill_response_klass
        'Bovaga::Bca::BillResponse'
      end

      def payment_response_klass
        'Bovaga::Bca::PaymentResponse'
      end

      def transaction_date
        @transaction_date ||= params["TransactionDate"].in_time_zone rescue nil
      end

      def request_id
        @request_id ||= params["RequestID"] rescue nil
      end

      def customer_number
        @customer_number ||= params["CustomerNumber"] rescue nil
      end

      def bill_params
        params.
          transform_keys do |key|
            key.underscore
          end.
          permit(:company_code, :customer_number, :request_id, :channel_type,
            :transaction_date, :additional_data)
      end

      def payment_params
        params.
          transform_keys do |key|
            key.underscore
          end.
          permit(:company_code, :customer_number, :request_id, :channel_type,
            :customer_name, :currency_code, :paid_amount, :total_amount,
            :sub_company, :transaction_date, :reference,
            :flag_advice, :additional_data, :detail_bills).
          merge(bill_details: bill_details_params)
      end

      def bill_details_params
        params.delete("DetailBills").collect do |param|
          param.
            transform_keys do |key|
              key.underscore
            end.
            permit(:bill_amount, :bill_number, :bill_sub_company, :bill_reference)
        end
      end

      def request_type_from_path(path)
        if path.include?('/bill')
          :bill
        elsif path.include?('/payment')
          :payment
        end
      end

      def validate_transaction_date
        if transaction_date && (transaction_date > Time.current)
          resp = { error: :bad_request }

          create_history(
            bank_code: 'BCA',
            kind_of: request_type_from_path(request.path),
            request: params,
            request_id: request_id,
            response: resp,
            messages: 'invalid_transaction_date')

          render json: resp, status: :bad_request
        end
      end
    end
  end
end
