# frozen_string_literal: true

module Bovaga
  class Mapping #:nodoc:
    attr_reader :singular, :path, :path_prefix, :bank_code, :capture_controller

    alias :name :singular

    def initialize(name, options) #:nodoc:
      @scoped_path = options[:as] ? "#{options[:as]}/#{name}" : name.to_s
      @singular = (options[:singular] || @scoped_path.tr('/', '_').singularize).to_sym

      @class_name = (options[:class_name] || name.to_s.classify).to_s
      @klass = Bovaga.ref(@class_name)

      @path = (options[:path] || name).to_s
      @path_prefix = options[:path_prefix]

      @bank_code = (options[:bank] || :base).to_s

      default_capture_controller(options)
    end

    # Gives the class the mapping points to.
    def to
      @klass.get
    end

    def fullpath
      "/#{@path_prefix}/#{@path}".squeeze("/")
    end

    def bank_adapter_klass
      [:bovaga, :bank_adapter, @bank_code].join('/').camelize
    end

    private

    def default_capture_controller(options)
      default_controller = [:bovaga, :capture, @bank_code].join('/')
      @capture_controller = options[:controller] || default_controller
    end
  end
end
