# frozen_string_literal: true

module Bovaga
  module Controllers
    module RequestHistoryConcern
      extend ::ActiveSupport::Concern

      def request_history_klass
        'Bovaga::RequestHistory'.constantize rescue nil
      end

      def skip_history?
        request_history_klass.blank?
      end

      def create_history(options = {})
        return if skip_history?

        bank_code = options.try(:[], :bank_code)
        kind_of = options.try(:[], :kind_of)
        req = options.try(:[], :request).try(:to_json)
        req_id = options.try(:[], :request_id)
        resp = options.try(:[], :response).try(:to_json)
        messages = options.try(:[], :messages)

        request_history_klass.create(
          bank_code: bank_code,
          remote_ip: request.remote_ip,
          original_url: request.original_url,
          kind_of: kind_of,
          request_id: req_id,
          request: req,
          response: resp,
          error_messages: messages
        )
      end
    end
  end
end
