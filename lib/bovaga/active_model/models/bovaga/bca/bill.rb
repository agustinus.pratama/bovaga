# frozen_string_literal: true

module Bovaga
  module Bca
    class Bill
      include ActiveModel::Model

      attr_accessor :company_code, :customer_number, :request_id, :channel_type,
        :transacted_at, :transaction_date, :additional_data

      validates :company_code,
        presence: true, length: { maximum: 5 }
      validates :customer_number,
        presence: true, length: { maximum: 18 }
      validates :request_id,
        presence: true, length: { maximum: 30 }
      validates :channel_type,
        presence: true, length: { maximum: 4 }
      validates :transacted_at,
        presence: true
      validates :transaction_date,
        format: { with: /\A([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?\z/ }
      validate :prefix_code_number

      # NOTE: Need to defined at main app
      def va_prefix_code
      end

      # NOTE: Need to defined at main app
      def va_type
      end

      # NOTE: Need to defined at main app
      def va_name
      end

      # NOTE: Need to defined at main app
      def va_total_amount
      end

      # NOTE: Need to defined at main app
      def va_items
      end

      # NOTE: Need to defined at main app
      def va_bill_valid?
      end

      # NOTE: Need to defined at main app
      def va_currency
      end

      def va_number
        customer_number
      end

      def request_parameter_valid?
        valid?
      end

      def active?
        (va_type == :no_bill) || va_bill_valid?
      end

      def all_error_messages
        errors.full_messages.to_sentence
      end

      def transacted_at
        @transacted_at ||= Date.parse(transaction_date) rescue nil
      end

      private

      def prefix_code_number
        unless company_code.eql?(va_prefix_code)
          errors.add(:company_code, :not_match, value: va_prefix_code)
        end
      end
    end
  end
end
