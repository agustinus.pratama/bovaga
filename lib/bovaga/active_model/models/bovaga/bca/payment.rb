# frozen_string_literal: true

require 'money'

module Bovaga
  module Bca
    class Payment
      include ActiveModel::Model

      attr_accessor :company_code, :customer_number, :request_id, :channel_type,
        :customer_name, :currency_code, :paid_amount, :total_amount, :amount,
        :transacted_at, :transaction_date, :flag_advice, :sub_company, :reference,
        :additional_data, :bill_details

      validates :company_code,
        presence: true,
        length: { maximum: 5 }
      validates :customer_number,
        presence: true,
        length: { maximum: 18 }
      validates :request_id,
        presence: true,
        length: { maximum: 30 }
      validates :channel_type,
        presence: true,
        length: { maximum: 4 }
      validates :customer_name,
        presence: true,
        length: { maximum: 30 }
      validates :currency_code,
        presence: true,
        length: { maximum: 3 }
      validates :paid_amount,
        presence: true
      validates :total_amount,
        presence: true
      validates :transacted_at,
        presence: true
      validates :flag_advice,
        presence: true,
        inclusion: { in: %w(Y N) }
      validates :sub_company,
        presence: true,
        length: { maximum: 5 }
      validates :reference,
        presence: true,
        length: { maximum: 15 }
      validates :transaction_date,
        format: { with: /\A([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?\z/ }
      validate :prefix_code_number
      validate :paid_amount_value
      validate :amount_value

      # NOTE: Need to defined at main app
      def va_prefix_code
      end

      # NOTE: Need to defined at main app
      def va_type
      end

      # NOTE: Need to defined at main app
      def va_name
      end

      # NOTE: Need to defined at main app
      def va_total_amount
      end

      # NOTE: Need to defined at main app
      def va_currency
      end

      # NOTE: Need to defined at main app
      def va_items
      end

      # NOTE: Need to defined at main app
      def va_bill_valid?
      end

      def va_number
        customer_number
      end

      def request_parameter_valid?
        valid?
      end

      def active?
        (va_type == :no_bill) || va_bill_valid?
      end

      def all_error_messages
        errors.full_messages.to_sentence
      end

      def transacted_at
        @transacted_at ||= Date.parse(transaction_date) rescue nil
      end

      # TODO: Add rate if using different currency
      def equal_amount?(first, second)
        return false unless first[:currency].eql?(second[:currency])

        first_money = Money.from_amount(first[:amount].to_d, first[:currency])
        second_money = Money.from_amount(second[:amount].to_d, second[:currency])

        first_money == second_money
      end

      private

      def prefix_code_number
        unless company_code.eql?(va_prefix_code)
          errors.add(:company_code, :not_match, value: va_prefix_code)
        end
      end

      def paid_amount_value
        case va_type
        when :fixed_amount
          errors.add(:paid_amount, :invalid) unless paid_amount.eql?(total_amount)
        end
      end

      def amount_value
        va_total_amount_params = { amount: va_total_amount, currency: va_currency }
        total_amount_params = { amount: total_amount, currency: currency_code }

        case va_type
        when :fixed_amount
          errors.add(:amount, :invalid) unless equal_amount?(va_total_amount_params, total_amount_params)
        end
      end
    end
  end
end
