# frozen_string_literal: true

module Bovaga
  module Bca
    class PaymentResponse
      attr_accessor :status, :payment

      def initialize(payment)
        self.payment = payment

        # Status default to failure
        self.status = {
          type: '01',
          key: :invalid
        }
      end

      def body
        {
          'CompanyCode' => company_code,
          'CustomerNumber' => customer_number,
          'RequestID' => request_id,
          'PaymentFlagStatus' => status_type,
          'PaymentFlagReason' => {
            'Indonesian' => I18n.t(status_key, scope: status_scope, locale: :id),
            'English' => I18n.t(status_key, scope: status_scope, locale: :en)
          },
          'CustomerName' => customer_name,
          'CurrencyCode' => currency_code,
          'PaidAmount' => paid_amount,
          'TotalAmount' => total_amount,
          'TransactionDate' => transaction_date,
          'DetailBills' => bill_details,
          'FreeTexts' => [],
          'AdditionalData' => ''
        }
      end

      def success!
        self.status = {
          type: '00',
          key: :success
        }
      end

      def error!(key)
        self.status = {
          type: '01',
          key: key
        }
      end

      def process(&block)
        if payment.request_parameter_valid?
          if payment.active?
            success!

            yield
          else
            error!(:timeout)
          end
        end
      end

      private

      # NOTE: This value is from request parameter
      def company_code
        payment.try(:company_code)
      end

      # NOTE: This value is from request parameter
      def customer_number
        payment.try(:customer_number)
      end

      # NOTE: This value is from request parameter
      def request_id
        payment.try(:request_id)
      end

      def status_type
        status.try(:[], :type)
      end

      def status_key
        status.try(:[], :key)
      end

      def status_scope
        'bca.payment.messages'
      end

      # NOTE: This value is from request parameter
      def customer_name
        payment.try(:customer_name)
      end

      # NOTE: This value is from bill model
      def currency_code
        payment.try(:currency_code) || Bovaga.default_currency
      end

      # NOTE: This value is from request parameter
      def paid_amount
        payment.try(:paid_amount)
      end

      # NOTE: This value is from request parameter
      def total_amount
        payment.try(:total_amount)
      end

      # NOTE: This value is from request parameter
      def transaction_date
        payment.try(:transaction_date)
      end

      # NOTE: Not used yet
      def bill_details
        (payment.try(:bill_details) || []).each do |item|
          bill_detail(item)
        end
      end

      # NOTE: Not used yet
      def bill_detail_number(item)
        item.try(:[], :bill_number) || ''
      end

      # NOTE: Not used yet
      def bill_detail(item)
        {
          'BillNumber' => bill_detail_number(item),
          'Status' => status_type,
          'Reason' => { # WARNING: Keep this under 50 characters
            'Indonesian' => I18n.t(status_key, scope: status_scope, locale: :id),
            'English' => I18n.t(status_key, scope: status_scope, locale: :en)
          }
        }
      end
    end
  end
end
