# frozen_string_literal: true

module Bovaga
  class RequestHistory < ApplicationRecord
    self.table_name = 'bovaga_request_histories'

    def request_json
      JSON.parse request
    end

    def response_json
      JSON.parse response
    end
  end
end
