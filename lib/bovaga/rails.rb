# frozen_string_literal: true

require 'bovaga/rails/routes'

module Bovaga
  class Engine < ::Rails::Engine
    config.bovaga = Bovaga

    # Force routes to be loaded if we are doing any eager load.
    config.before_eager_load do |app|
      app.reload_routes! if Bovaga.reload_routes
    end

    paths["app/models"] << "lib/bovaga/active_model/models"
  end
end
