# frozen_string_literal: true

class <%= class_name %>Controller < Bovaga::Capture::<%= options.bank_code.downcase.classify %>Controller
  protected

  def bill_klass
    # default: 'Bovaga::<%= options.bank_code.downcase.classify %>::Bill'
    "<%= [singular_name, '_bill'].join.classify %>"
  end

  def after_bill_success(bill)
  end

  def payment_klass
    # default: 'Bovaga::<%= options.bank_code.downcase.classify %>::Payment'
    "<%= [singular_name, '_payment'].join.classify %>"
  end

  def after_payment_success(payment)
  end
end
