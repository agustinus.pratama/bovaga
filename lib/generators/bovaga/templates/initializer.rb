# frozen_string_literal: true

<%- unless options.skip_history -%>
require 'bovaga/active_record/models/bovaga/request_history'
<%- end -%>

Bovaga.setup do |config|
  # ==> Controller configuration
  # Configure the parent class to the bovaga controllers.
  # config.parent_controller = 'ApplicationController'

  # When false, Bovaga will not attempt to reload routes on eager load.
  # This can reduce the time taken to boot the app but if your application
  # requires the Bovaga mappings to be loaded during boot time the application
  # won't boot properly.
  # config.reload_routes = true

  # Define other whitelisted ips
  # config.whitelisted_ips = []
end
